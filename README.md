# README #

This is a Robospice sample tweaked to access the Just Eat api.  This is not how I usually write code. This is how Stephane Nicolas (Robospice author) writes code. It lacks my usual decoupled architecture and unit tests.  This is all I had time for.  As such, it doesn't really showcase my coding abilities.  It just proves that I can knock-up an app in a short amount of time.  It simply ticks a box in recruitment process.

### Other things I didn't have time for ###

See `Answers to technical questions.md`.

### How do I get set up? ###

* The original project was a Maven project so this is a Maven project.
* I didn't have time to write Gradle build scripts.
* Eclipse imported this beautifully with its' Maven plugin. Studio failed miserably.
* I suspect 'maven build' will create an apk in the 'target' folder if you have maven command-line - although I haven't tried it.