package com.octo.android.robospice.sample.ui.spicelist.model;


public class Logo {
    public String StandardResolutionURL;

    @Override
    public boolean equals(Object obj) {
        Logo other = (Logo) obj;
        if (StandardResolutionURL == null) {
            if (other.StandardResolutionURL != null) {
                return false;
            }
        } else if (other == null) {
            return false;
        } else if (!StandardResolutionURL.equals(other.StandardResolutionURL)) {
            return false;
        }
        return true;
    }
}
