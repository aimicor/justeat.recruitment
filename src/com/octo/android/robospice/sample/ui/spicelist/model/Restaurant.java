package com.octo.android.robospice.sample.ui.spicelist.model;

import java.util.ArrayList;

public class Restaurant {
    private ArrayList<Logo> Logo;
    private ArrayList<CuisineTypes> CuisineTypes;
    private String Name;
    private double RatingStars;

    public String getName() {
        return Name;
    }

    public String getGravatar_id() {
        if (Logo != null && Logo.size() > 0) {
            return Logo.get(0).StandardResolutionURL;
        }
        return null;
    }

    public double getScore() {
        return RatingStars;
    }

    public String getCuisine() {
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i<CuisineTypes.size();i++){
            builder.append(CuisineTypes.get(i).Name);
            if (i < CuisineTypes.size() - 1) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }
}