package com.octo.android.robospice.sample.ui.spicelist.adapter;

import java.io.File;

import android.content.Context;
import android.view.ViewGroup;

import com.octo.android.robospice.request.okhttp.simple.OkHttpBitmapRequest;
import com.octo.android.robospice.sample.ui.spicelist.model.Restaurant;
import com.octo.android.robospice.sample.ui.spicelist.model.Restaurants;
import com.octo.android.robospice.sample.ui.spicelist.view.GitHubUserView;
import com.octo.android.robospice.spicelist.SpiceListItemView;
import com.octo.android.robospice.spicelist.okhttp.OkHttpBitmapSpiceManager;
import com.octo.android.robospice.spicelist.okhttp.OkHttpSpiceArrayAdapter;
import com.octo.android.robospice.spicelist.simple.SpiceArrayAdapter;

/**
 * An example {@link SpiceArrayAdapter}.
 * @author jva
 * @author stp
 * @author sni
 */
public class GitHubUserListAdapter extends OkHttpSpiceArrayAdapter<Restaurant> {

    // --------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    // --------------------------------------------------------------------------------------------

    public GitHubUserListAdapter(Context context, OkHttpBitmapSpiceManager spiceManagerBitmap, Restaurants users) {
        super(context, spiceManagerBitmap, users.getUsers());
    }

    @Override
    public OkHttpBitmapRequest createRequest(Restaurant gitHubUser, int imageIndex, int requestImageWidth, int requestImageHeight) {
        File tempFile = new File(getContext().getCacheDir(), "THUMB_IMAGE_TEMP_" + gitHubUser.getName());
        return new OkHttpBitmapRequest(gitHubUser.getGravatar_id(), requestImageWidth,
                requestImageHeight, tempFile);
    }

    @Override
    public SpiceListItemView<Restaurant> createView(Context context, ViewGroup parent) {
        return new GitHubUserView(getContext());
    }
}
