package com.octo.android.robospice.sample.ui.spicelist.network;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.octo.android.robospice.sample.ui.spicelist.model.Restaurants;

public class GitHubRequest extends SpringAndroidSpiceRequest<Restaurants> {

    private String location;

    public GitHubRequest(String location) {
        super(Restaurants.class);
        this.location = location;
    }

    @Override
    public Restaurants loadDataFromNetwork() throws Exception {
        String url = "http://api-interview.just-eat.com/restaurants?q=" + location;

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept-Tenant", "uk");
        headers.add("Accept-Language", "en-GB");
        headers.add("Authorization", "Basic VGVjaFRlc3RBUEk6dXNlcjI=");

        ResponseEntity<Restaurants> response = getRestTemplate().exchange(url, HttpMethod.GET, new HttpEntity(headers), Restaurants.class);
        return response.getBody();
    }
}
