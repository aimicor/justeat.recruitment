####1. How long did you spend on the coding test? What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.

About a day.  A third of that was because the Jackson parser wasn't working (I assumed it was my model setup).  In the end I swapped it for Gson and everything was fine.

I would have done the following given more time:

* Implement some sort of dependancy injection to facilitate unit testing.
* Write some unit tests.
* Move nested classes to their own files.
* The original sample app showed a list of Github users.  There are still code references to Github everywhere.
* Change the package name.
* Change the action bar title.
* Integrate the GPS location services (requested but not in the acceptance criteria).
* The stock rating indicator is not very clear alternating between shades of grey and cyan. I didn't have time to style my own.
* Some of the logos aren't getting rendered. I imagine the Robospice bitmap fetcher isn't very good. No time to integrate Picasso.
* Put a SearchView in the action bar so that you can enter a different postcode (not in the acceptance criteria).
* Complete the data models.
* Make the data models Parcelable so that they can easily be passed into a Details fragment.

####2. What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.

ActionBarDrawerToggle. This class makes implementation of the navigation drawer significantly easier - although the constructor forces you to break any kind of MVC split you might have. It may not have been added to the *very* latest version but I only found out about it recently.

```java
mDrawerLayout = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
mActionBarDrawerToggle = new ActionBarDrawerToggle(
            activity,               /* host Activity */
            mDrawerLayout,          /* DrawerLayout object */
            R.string.drawer_open,   /* "open drawer" description for accessibility */
            R.string.drawer_close   /* "close drawer" description for accessibility */
mActionBarDrawerToggle = mToggleFactory.getActionBarDrawerToggle(activity, mDrawerLayout);
mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
mActionBarDrawerToggle.addActionBarDrawerToggleListener(this);
```

The latest version has deprecated the built-in action bar tabs and forces you to write your own tool bar under certain circumstances which is a step backwards in my opinion.

####3. How would you track down a performance issue in an application? Have you ever had to do this?

* If it's UI performance then you get info messages about specific methods taking too long on the main thread in logcat.
* If it's memory performance then the memory profiler in DDMS helps show up leaks.

####4. How would you improve the JUST EAT APIs that you just used?

I misinterpreted this question the first time around due to the rush of getting something to the client. It's now 19 months later and my memory is a little foggy. I remember that there was a lot of superfluous data return in the json body - probably only needed about 10% of it. The biggest mistake wrt apis that I have come across is that they are generally written and optimised for websites. In contrast to the Just-Eat apis, the minimum data is returned and you need to make follow-up calls to get the level of data required. For a mobile device, the time to download information, especially if it is json text, is a small fraction compared to the time required to establish a connection. If you have to make a lot of connections, the perception of the app resonse time to the user becomes very poor.  Therefore, more data per call is generally better.

####5. Please describe yourself using JSON.

```json
{
	"name" : "Myles"
	"surname" : "Bennett"
	"occupation" : "Android dev lead"
	"phone" : "07495301005"
	"email" : "myles.bennett@aimicor.com"
	"url" : "http://www.aimicor.com"
}
```